package LazyClass;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class FooSaver {

	//single functionality of the class - not ideal
	private static void save(int id) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(id+".txt")) {
		    out.println(id);
		}
	}
}
