package FeatureEnvy;

public class access {
	
	Rectangle rectangle = new Rectangle();
	Circle circle = new Circle();
	

	//feature envy - should belong to Rectangle
	private double rectangleArea() {
		rectangle.setHeight(10);
		rectangle.setWidth(20);
		return rectangle.getHeight() * rectangle.getWidth();
	}
	
	//feature envy - should belong to Circle
	private void circleArea() {
		circle.setRadius(5);
		circle.draw(circle.getRadius());
	}
}
