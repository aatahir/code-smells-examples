package FeatureEnvy;

public class StudentDetails {

	
	static Student student = new Student();
	
	public static void main(String[] args) {
		student.setName("Alex");
		student.setID(1100);
		student.setMajor("CS");
		System.out.println(StudentDetails.getStudentDetails());
	}
	
	public static String getStudentDetails()
	{
		return student.getName() + " " + student.getID() + " "+ student.getMajor();
	}
}
