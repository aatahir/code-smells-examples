package FeatureEnvy;

import java.lang.Math.*;

public class Circle {

	private int radius;
	
	public Circle(int radius) {
		this.radius = radius;
	}

	public Circle() {
		// TODO Auto-generated constructor stub
	}
	


	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

	public void draw(int radius) {
		System.out.println("Size of circle is = " +CircleArea(radius));
	}

	private double CircleArea(int radius) {
		return Math.PI * (Math.pow(radius, 2));
	}

}
