package CircularDependencies;

public class Student {
	
	String getStudentFN(){
		return "Alex";
	}
	
	String getDetails(){
		Course course = new Course();
		String c= course.getCourse();

		Degree degree = new Degree();
		String d = degree.getDegree();
		
		return this.getStudentFN() + " " + c + " "+ d; 
	}
}
