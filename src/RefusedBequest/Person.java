package RefusedBequest;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Person {

	private String name;
	private int id;

	public void displayName(String name) { System.out.println(this.name); }
	
	public void displayID(int id) { System.out.println(this.id); }
	
	private static void export(String name, int id) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter("Students.txt")) {
		    out.println("Student "+ name + " ID: "+ id );
		}
	}
}
