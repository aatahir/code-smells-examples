package deadcode;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Foo {
	
	String name = "x";

	public static void main(String[] args) throws FileNotFoundException {
		int x = 10;
		int y = 20;
		
		//add x+y and add and pass the value to the save(int) method
		while (x>y)
		{
			int z = x + y; 
			Foo.save(z);
			x++;
		}
		System.out.print("done!");
		
	}

	//create a new text file and add the value inside the file
	private static void save(int id) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter(id+".txt")) {
		    out.println(id);
		}
	}
	

}
