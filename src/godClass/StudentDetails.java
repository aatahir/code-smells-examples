package godClass;

public class StudentDetails {

	static Student student = new Student();

	public static void main(String[] args) {
		student.setName("Alex");
		student.setID(1100);
		student.setMajor("CS");
		System.out.println(StudentDetails.getStudentDetails());
	}

	public static String getStudentDetails() {
		return student.getName() + " " + student.getID() + " " + student.getMajor();
	}

	void printDetails(int id) {
	}

	void addStudent(String name, int id) {
	}

	void deleteStudent(int id) {
	}

	void updateStudent(int id) {
	}

	void registerCourse(String name, int id) {
	}

	String getCourse(int id) {
		String x = "";
		return x;
	}

	void login(int id) {
	}

	void logout(int id) {
	}

	void addRecords(String name, int id) {
	}

	void saveIntoFile(String name, int id) {
	}

}
