package duplication;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Student {
	public String name;
	public int id;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAddress() {
		return id;
	}
	public void setAddress(int id) {
		this.id = id;
	}

	private static void save(String name, int id) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter("Students.txt")) {
		    out.println("Student "+ name + " ID: "+ id );
		}
	}
}
