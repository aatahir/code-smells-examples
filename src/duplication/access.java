package duplication;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;

public class access {

	public static void main(String[] args) throws FileNotFoundException {
		Student student = new Student();
		student.name= "Spock";
		student.id = 1100;
		
		access.save(student.name,student.id);
		
		access.export(student.name,student.id);
		
		
	}

	// logical duplication with static void save(String name, int id)
	private static void export(String name, int id) throws FileNotFoundException {
		try (PrintStream out = new PrintStream(new FileOutputStream("names.txt"))) {
		    out.print("ID: "+ id+ ": Name: "+ name );
		}		
	}

	// logical duplication with static void export(String name, int id)
	private static void save(String name, int id) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter("Students.txt")) {
		    out.println("Student "+ name + " ID: "+ id );
		}
	}
	
	

}
