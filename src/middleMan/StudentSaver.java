package middleMan;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class StudentSaver {

	private static void save(String name, int id) throws FileNotFoundException {
		try (PrintWriter out = new PrintWriter("Students.txt")) {
		    out.println("Student "+ name + " ID: "+ id );
		}
	}
}
