package TestSmells;

import static org.junit.Assert.*;

import org.junit.Test;

public class StudentTest {

	
	// assertion-free test
	@Test
	public void testStudentDetails() {
		StudentDetails sd = new StudentDetails();
		Student s = new Student();

		s.setName("Alex");
		s.setID(1100);
		s.setMajor("CS");

		sd.getStudentDetails();
	}
	
	
	// Eager test
	@Test
	public void testStudentDetails2() {
		StudentDetails sd = new StudentDetails();
		Student s = new Student();

	
	}
	

}
